import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

/**
 * Generate input data set for genetic algorithm process
 * Automatically generate cluster list data set 
 * based on phone's specification
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class InputGenerator {
	
	/*********************
	 * Constructor
	 **********************/
	public InputGenerator()
	{
		
	}
	
	/*********************
	 * Generate cluster
	 * @return		Clustering		a cluster with random selected specification
	 **********************/
	public Clustering generateCluster(int totalSpecNumber)
	{
		/* original data */ 
		PhoneData phones = new PhoneData();
		
		/* set random specification data list */ 
		Random random = new Random();
		List<String> randomSpec = new ArrayList<String>();
		for(int i = 0; i < totalSpecNumber ; i++)
		{
			int n = random.nextInt(4); // 75% select a specific specification
			if( n > 1)
				randomSpec.add(phones.spec[i]) ;
		}
		String[] newSpec = new String[randomSpec.size()];
		randomSpec.toArray(newSpec);
		
		/* set new specs in the cluster */
		Clustering myClustering = new Clustering(randomSpec.size(), newSpec);
		for(int i = 0 ; i < 22 ; i++)
		{
			myClustering.addBitSet(phones.name[i], convertBitSetByNewSpec(phones.dataBitSet[i], phones.spec, totalSpecNumber, newSpec));
		}
		// run bi-clustering
		myClustering.runAlgorithm(myClustering.getData() , true);
		return myClustering;
	}
	
	/*********************
	 * Generate cluster
	 * @return		Clustering		a cluster with random selected specification
	 **********************/
	public Clustering generateBiCluster(int totalSpecNumber, int mode)
	{
		/* original data */ 
		PhoneData phones = new PhoneData();
		
		/* set random specification data list */ 
		Random random = new Random();
		List<String> randomSpec = new ArrayList<String>();
		for(int i = 0; i < totalSpecNumber ; i++)
		{
			int n = random.nextInt(4); // 75% select a specific specification
			if( n > 1)
				randomSpec.add(phones.spec[i]) ;
		}
		String[] newSpec = new String[randomSpec.size()];
		randomSpec.toArray(newSpec);
		
		/* set new specs in the cluster */
		Clustering myClustering = new Clustering(randomSpec.size(), newSpec);
		for(int i = 0 ; i < 22 ; i++)
		{
			myClustering.addBitSet(phones.name[i], convertBitSetByNewSpec(phones.dataBitSet[i], phones.spec, totalSpecNumber, newSpec));
		}
//		
//		boolean[][] data = new boolean[myClustering.myData.size()][myClustering.spec.length];
//		for(int i = 0; i < myClustering.myData.size() ; i++)
//		{
//			for(int j = 0; j < myClustering.spec.length; j++)
//			{
//				data[i][j] = myClustering.myData.get(i).data.get(j);
//				//System.out.print(data[i][j] + " ");
//			}
//		}
//		
//		
//		// run bi-clustering
//		// myClustering.runAlgorithm(myClustering.getData() , true);
//		BiClustering biCluster = new BiClustering(data, myClustering.myData.size(), myClustering.spec.length);
//		//biCluster.runAlgorithm(mode);
//		biCluster.runAlgorithm(mode);
//		
//		String[] specs_list = new String[biCluster.specs_list.length];
//		for(int i = 0 ; i < biCluster.specs_list.length ; i++)
//			specs_list[i] = Integer.toString(biCluster.specs_list[i]);
//		
//		Clustering resultCluster= new Clustering(biCluster.specs_size, specs_list);
//		
//		for(int i = 0; i < myClustering.myData.size() ; i++)
//		{
//			BitSet outputTmp = new BitSet();
//			for(int j = 0; j < myClustering.spec.length; j++)
//			{
//				if(data[i][j] == true)
//					outputTmp.set(j, true);
//			}
//			outputTmp.set(newSpec.length, true);
//			resultCluster.addBitSet(Integer.toString(biCluster.phones_list[i]), convertToLong(outputTmp));
//		}
		
		return convertClusterByBiCluster(myClustering, 1, newSpec, phones.name);
	}
	
	public static Clustering convertClusterByBiCluster(Clustering myClustering, int mode, String[] newSpec, String[] phones_list)
	{
		boolean[][] data = new boolean[myClustering.myData.size()][myClustering.spec.length];
		for(int i = 0; i < myClustering.myData.size() ; i++)
		{
			for(int j = 0; j < myClustering.spec.length; j++)
			{
				data[i][j] = myClustering.myData.get(i).data.get(j);
			}
		}
		// run bi-clustering
		BiClustering biCluster = new BiClustering(data, myClustering.myData.size(), myClustering.spec.length);
		biCluster.specs_list = myClustering.spec;
		//PhoneData phonData = new PhoneData();
		biCluster.phones_list = phones_list;
//		for(int i = 0; i < biCluster.specs_list.length ; i++)
//		{
//			System.out.print(biCluster.specs_list[i] + " ");
//		}
//		System.out.println();
		biCluster.runAlgorithm(mode, false);
//		for(int i = 0; i < biCluster.specs_list.length ; i++)
//		{
//			System.out.print(biCluster.specs_list[i] + " ");
//		}
//		System.out.println();
		
		Clustering resultCluster= new Clustering(biCluster.specs_size, biCluster.specs_list);
		
		for(int i = 0; i < myClustering.myData.size() ; i++)
		{
			BitSet outputTmp = new BitSet();
			for(int j = 0; j < myClustering.spec.length; j++)
			{
				if(data[i][j] == true)
					outputTmp.set(j, true);
			}
			outputTmp.set(newSpec.length, true);
			resultCluster.addBitSet(biCluster.phones_list[i], convertToLong(outputTmp));
//			System.out.print(biCluster.phones_list[i] + " ");
//			Printer.printData(outputTmp);
//			System.out.println();
		}
		
//		Printer.printOriginalData(biCluster.specs_list, resultCluster.myData);
		
		for(int i = 0 ; i < biCluster.phone_size ; i++)
		{
			resultCluster.resultNames.add(biCluster.phones_list[i]);
		}
		
		for(int i = 0; i < biCluster.specs_size ; i++)
		{
			BitSet outputTmp = new BitSet();
			for(int j = 0; j < biCluster.phone_size; j++)
			{
				if(data[j][i] == true)
					outputTmp.set(j, true);
			}
			outputTmp.set(biCluster.phone_size, true);
			resultCluster.addResultBitSet(biCluster.specs_list[i], convertToLong(outputTmp));
		}
		
		return resultCluster;
	}
	
	public static Clustering convertClusterByBiClusterReassamble(int mode)
	{
		PhoneData phoneData = new PhoneData();
		Clustering myClustering = new Clustering(phoneData.spec.length, phoneData.spec);
		for(int i = 0 ; i < 22 ; i++)
		{
			myClustering.addBitSet(phoneData.name[i], phoneData.dataBitSet[i]);
		}
		boolean[][] data = new boolean[myClustering.myData.size()][myClustering.spec.length];
		for(int i = 0; i < myClustering.myData.size() ; i++)
		{
			for(int j = 0; j < myClustering.spec.length; j++)
			{
				data[i][j] = myClustering.myData.get(i).data.get(j);
			}
		}
		// run bi-clustering
		BiClustering biCluster = new BiClustering(data, myClustering.myData.size(), myClustering.spec.length);
		biCluster.specs_list = myClustering.spec;
		
		biCluster.phones_list = phoneData.name;
		biCluster.runAlgorithm(mode, false);
		
		Clustering resultCluster= new Clustering(biCluster.specs_size, biCluster.specs_list);
		
		for(int i = 0; i < myClustering.myData.size() ; i++)
		{
			BitSet outputTmp = new BitSet();
			for(int j = 0; j < myClustering.spec.length; j++)
			{
				if(data[i][j] == true)
					outputTmp.set(j, true);
			}
			outputTmp.set(myClustering.spec.length, true);
			resultCluster.addBitSet(biCluster.phones_list[i], convertToLong(outputTmp));
		}
		
		for(int i = 0 ; i < biCluster.phone_size ; i++)
		{
			resultCluster.resultNames.add(biCluster.phones_list[i]);
		}
		
		for(int i = 0; i < biCluster.specs_size ; i++)
		{
			BitSet outputTmp = new BitSet();
			for(int j = 0; j < biCluster.phone_size; j++)
			{
				if(data[j][i] == true)
					outputTmp.set(j, true);
			}
			outputTmp.set(biCluster.phone_size, true);
			resultCluster.addResultBitSet(biCluster.specs_list[i], convertToLong(outputTmp));
		}
		return resultCluster;
	}
	
//	/*********************
//	 * Generate reassamble cluster
//	 * @return		Clustering		a cluster with random selected specification
//	 **********************/
//	public Clustering generateReassambleCluster(int totalSpecNumber)
//	{
//		/* original data */ 
//		PhoneData phones = new PhoneData();
//		Clustering clustering = new Clustering(35, phones.spec);
//		// set data
//		for(int i = 0 ; i < 22 ; i++)
//		{
//			clustering.addBitSet(phones.name[i], phones.dataBitSet[i]);
//		}
//		clustering.runAlgorithm(clustering.getData() , true);
//		Clustering newClusterTmp = GeneticAlgorithm.convertToHorizontal(clustering.resultNames, clustering.resultBiCluster, GeneticAlgorithm.setSpec(clustering.resultBiCluster));
//		
//		/* set random specification data list */ 
//		Random random = new Random();
//		List<DataSet> randomDataSet = new ArrayList<DataSet>();
//		for(int i = 0; i < totalSpecNumber ; i++)
//		{
//			if(i >= 0 && i <= 5)
//			{
//				randomDataSet.add(reassamble(0, 5, newClusterTmp.myData));
////				int n = random.nextInt(4); // 75% select a specific specification
////				if( n > 1)
////					randomSpec.add(phones.spec[i]) ;
//			}else if(i >= 6 && i <= 11)
//			{
//				
//			}else if(i >= 12 && i <= 14)
//			{
//				
//			}else if(i >= 15 && i <= 18)
//			{
//				
//			}else if(i >= 20 && i <= 22)
//			{
//				
//			}else if(i >= 23 && i <= 34)
//			{
//				
//			}
//			
//			int n = random.nextInt(4); // 75% select a specific specification
//			if( n > 1)
//				randomSpec.add(phones.spec[i]) ;
//		}
//		String[] newSpec = new String[randomSpec.size()];
//		randomSpec.toArray(newSpec);
//		
//		/* set new specs in the cluster */
//		Clustering myClustering = new Clustering(randomSpec.size(), newSpec);
//		for(int i = 0 ; i < 22 ; i++)
//		{
//			myClustering.addBitSet(phones.name[i], convertReassambleBitSetByNewSpec(phones.dataBitSet[i], phones.spec, totalSpecNumber, newSpec));
//		}
//		// run bi-clustering
//		myClustering.runAlgorithm(myClustering.getData() , true);
//		return myClustering;
//	}
//	
//	/*********************
//	 * Generate reassamble cluster
//	 * @return		Clustering		a cluster with random selected specification
//	 **********************/
//	public DataSet reassamble(int a, int b, List<DataSet> data)
//	{
//		String str = "";
//		Random random = new Random();
//		int n = random.nextInt(b - a);
//		str = data.spec[a + n]; 
//		for(int i = 0; i < (b - a); i++)
//		{
//			n = random.nextInt(3);
//			if(n == 0)
//			{
//				n = random.nextInt(b - a);
//				str = str + "," + data.spec[a + n];
//			}
//		}
//		return str;
//	}
	
	/*************
	 * generate new input data set by new specification
	 * @param	input		long 
	 * @param	orgSpec		original specification list
	 * @param	newSpec		new specification list
	 * @return	long		the new BitSet followed by new specification
	 * **********/
	private long convertReassambleBitSetByNewSpec(long input, String[] orgSpec, int orgSpecLength, String[] newSpec)
	{
		long output = 0;
		BitSet inputTmp = convert(input);
		BitSet outputTmp = new BitSet();
		int index = newSpec.length - 1;
		int j = 0;
		
		/******************************/
		for(int i = 0; i <  orgSpecLength ; i++ )
		{
			String[] str = newSpec[j].split(",");
			if(str.length == 1)
			{
				if(orgSpec[i].equals(newSpec[j]))
				{
					outputTmp.set(index, inputTmp.get(orgSpecLength - 1 - i));
					index--;
					j++;
					if(j >= newSpec.length)
						break;
				}
			}else
			{
				for(int k = 0 ; k < str.length ; k++)
				{
					if(orgSpec[i].equals(newSpec[j]))
					{
						outputTmp.set(index, inputTmp.get(orgSpecLength - 1 - i));
						index--;
						j++;
						if(j >= newSpec.length)
							break;
					}
				}
			}
			
			
		}
		outputTmp.set(newSpec.length, true);
		output = convertToLong(outputTmp);
		
		return output;
	}
	
	/*************
	 * generate new input data set by new specification
	 * @param	input		long 
	 * @param	orgSpec		original specification list
	 * @param	newSpec		new specification list
	 * @return	long		the new BitSet followed by new specification
	 * **********/
	private long convertBitSetByNewSpec(long input, String[] orgSpec, int orgSpecLength, String[] newSpec)
	{
		long output = 0;
		BitSet inputTmp = convert(input);
		BitSet outputTmp = new BitSet();
		int index = newSpec.length - 1;
		int j = 0;
		
		/******************************/
		for(int i = 0; i <  orgSpecLength ; i++ )
		{
			if(orgSpec[i].equals(newSpec[j]))
			{
				outputTmp.set(index, inputTmp.get(orgSpecLength - 1 - i));
				index--;
				j++;
				if(j >= newSpec.length)
					break;
			}
		}
		outputTmp.set(newSpec.length, true);
		output = convertToLong(outputTmp);
		
		return output;
	}
	
	/*************
	 * convert long to BitSet
	 * @param	data		long 
	 * @return	BitSet		the BitSet converted from long
	 * **********/
	public static BitSet convert(long data)
	{
		BitSet tmp = new BitSet(36);
		int index = 0;
	    while (data != 0L) 
	    {
		    if (data % 2L != 0) 
		    {
		      tmp.set(index);
		    }
		    ++index;
		    data = data >>> 1;
	    }
		return tmp;
	}
	
	/*************
	 * convert BitSet to long
	 * @param	data		BitSet 
	 * @return	long		the long converted from BitSet
	 * **********/
	public static long convertToLong(BitSet bitset) 
	{
	    long value = 0L;
	    for (int i = 0; i < bitset.length(); ++i) 
	    {
	      value += bitset.get(i) ? (1L << i) : 0L;
	    }
	    return value;
	}
}
