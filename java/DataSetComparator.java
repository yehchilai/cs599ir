import java.util.Comparator;

/**
 *  compare to DataSet by their id
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class DataSetComparator  implements Comparator<DataSet> {
	/***********************
	 *  DataSet comparator comparator to sort data id
	 * 
	 * @param	o1	a DataSet 
	 * @param	o2	a DataSet
	 * @return	int	the result of comparator
	 * **********************/
	@Override
	public int compare(DataSet o1, DataSet o2) {
		// TODO Auto-generated method stub
		return o1.id.compareTo(o2.id);
	}
}
