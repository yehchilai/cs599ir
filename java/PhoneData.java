/**
 *   Data.
 *   The data is used to test our algorithm.
 *   Users can manually put new data here
 */

/**
 * @author MarkLai
 *
 */
public class PhoneData {
	
	public long[] dataBitSet = new long[100];
	public String[] name = new String[22];
	public String[] spec = new String[35];
	
	/*******
	 * Test Data
	 * 						Data : Body (weight)                            ; Display (inch)                    ; memory (RAM) ; Camera (Pixel)          ; NFC ; OS               ; Battery (mA)
	 * 							 :  110g , 130g , 145g , 150g , 160g , 175g ; 4.0 , 4.5 , 4.7 , 5.1 , 5.5 , 5.7 ; 1G , 2G , 3G ; 5Mp , 8Mp , 13Mp , 16Mp+; NFC ; a442 , a444 , i8 ; 1400 , 1500 , 1600 , 1800 , 1900 , 2000, 2200, 2400, 2600, 2800, 3000, 3200 ;
	 * Samsung Galaxy S5 Plus	 :	0	 , 0    , 1    , 0    , 0    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 0    , 1    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 1   , 0   , 0    ;
	 * Samsung Galaxy Note 4 Duos:	0	 , 0    , 0	   , 0    , 0    , 1	; 0   , 0   , 0   , 0   , 0   , 1   ; 0  , 0  , 1  ; 0   , 0   , 0    , 1    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 1   , 0    ;
	 * Samsung Galaxy A3		 :	1	 , 0    , 0	   , 0    , 0    , 0	; 0   , 1   , 0   , 0   , 0   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 1    , 0   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * Samsung Galaxy Note 4	 :	0	 , 0    , 0	   , 0    , 0    , 1	; 0   , 0   , 0   , 0   , 0   , 1   ; 0  , 0  , 1  ; 0   , 0   , 0    , 1    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 1    ;
	 * 
	 * HTC One (M8 Eye)			 :	0	 , 0    , 0	   , 0    , 1    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 1    , 0    ; 0   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 1   , 0   , 0   , 0    ;
	 * HTC Desire Eye			 :	0	 , 0    , 0	   , 1    , 0    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 1    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 1   , 0   , 0   , 0   , 0    ;
	 * HTC Butterfly 2			 :	0	 , 0    , 0	   , 1    , 1    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 1    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 1   , 0   , 0   , 0    ;
	 * HTC Desire 510			 :	0	 , 0    , 0	   , 0    , 1    , 0	; 0   , 1   , 0   , 1   , 0   , 0   ; 1  , 0  , 0  ; 1   , 0   , 0    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 1   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * HTC One (M8)				 :	0	 , 0    , 0	   , 0    , 1    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 0  , 1  ; 1   , 0   , 0    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 1   , 0   , 0   , 0    ;
	 * 
	 * Apple iPhone 6 Plus		 :	0	 , 0    , 0    , 0    , 0    , 1	; 0   , 0   , 0   , 0   , 1   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 1   ; 0    , 0    , 1  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 1   , 0    ;
	 * Apple iPhone 6			 :	0	 , 1    , 0    , 0    , 0    , 0	; 0   , 0   , 1   , 0   , 0   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 1   ; 0    , 0    , 1  ; 0    , 0    , 0    , 1    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * Apple iPhone 5s			 :	1	 , 0    , 0    , 0    , 0    , 0	; 1   , 0   , 0   , 0   , 0   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 0   ; 0    , 0    , 1  ; 0    , 0    , 1    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * Apple iPhone 5c			 :	0	 , 1    , 0    , 0    , 0    , 0	; 1   , 0   , 0   , 0   , 0   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 0   ; 0    , 0    , 1  ; 0    , 1    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * Apple iPhone 5			 :	1	 , 0    , 0    , 0    , 0    , 0	; 1   , 0   , 0   , 0   , 0   , 0   ; 1  , 0  , 0  ; 0   , 1   , 0    , 0    ; 0   ; 0    , 0    , 1  ; 1    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 0    ;
	 * 
	 * LG G3					 :	0	 , 0    , 0    , 1    , 0    , 0	; 0   , 0   , 0   , 0   , 1   , 0   ; 0  , 0  , 1  ; 0   , 0   , 1    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 1   , 0    ;
	 * LG G2					 :	0	 , 0    , 1    , 0    , 0    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 1    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 1   , 0    ;
	 * LG Nexus 5				 :	0	 , 1    , 0    , 0    , 0    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 1   , 0    , 0    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 1   , 0   , 0   , 0   , 0    ;
	 * LG Nexus 4				 :	0	 , 1    , 0    , 0    , 0    , 0	; 0   , 0   , 1   , 0   , 0   , 0   ; 0  , 1  , 0  ; 0   , 1   , 0    , 0    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 1   , 0   , 0   , 0   , 0   , 0    ;
	 * 
	 * Sony Experia Z1			 :	0	 , 0    , 0    , 0    , 0    , 1	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 0    , 1    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 1   , 0    ;
	 * Sony Experia Z2			 :	0	 , 0    , 0    , 0    , 1    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 0  , 1  ; 0   , 0   , 0    , 1    ; 1   ; 1    , 0    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 1    ;
	 * Sony Experia Z3			 :	0	 , 0    , 0    , 1    , 0    , 0	; 0   , 0   , 0   , 1   , 0   , 0   ; 0  , 0  , 1  ; 0   , 0   , 0    , 1    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 0   , 0   , 0   , 1    ;
	 * Sony Experia Z3 Compact	 :	0	 , 1    , 0    , 0    , 0    , 0	; 0   , 1   , 0   , 0   , 0   , 0   ; 0  , 1  , 0  ; 0   , 0   , 0    , 1    ; 1   ; 0    , 1    , 0  ; 0    , 0    , 0    , 0    , 0    , 0   , 0   , 0   , 1   , 0   , 0   , 0    ;
	 * **/
	
	public PhoneData()
	{
		// add data
		name[0] = "Samsung Galaxy S5 Plus";
		dataBitSet[0] = 0b100100000010001000011100000000000100L;
		name[1] = "Samsung Galaxy Note 4 Duos";
		dataBitSet[1] = 0b100000100000100100011010000000000010L;
		name[2] = "Samsung Galaxy A3";
		dataBitSet[2] = 0b110000001000010001001010000010000000L;
		name[3] = "Samsung Galaxy Note 4";
		dataBitSet[3] = 0b100000100000100100011010000000000001L;
		name[4] = "HTC One (M8 Eye)";
		dataBitSet[4] = 0b100001000010001000100010000000001000L;
		name[5] = "HTC Desire Eye";
		dataBitSet[5] = 0b100010000010001000101100000000010000L;
		name[6] = "HTC Butterfly 2";
		dataBitSet[6] = 0b100011000010001000101100000000001000L;
		name[7] = "HTC Desire 510";
		dataBitSet[7] = 0b100001001010010010001100000001000000L;
		name[8] = "HTC One (M8)";
		dataBitSet[8] = 0b100001000010000110001100000000001000L;
		name[9] = "Apple iPhone 6 Plus";
		dataBitSet[9] = 0b100000100001010001001001000000000010L;
		name[10] = "Apple iPhone 6";
		dataBitSet[10] = 0b101000000100010001001001000100000000L;
		name[11] = "Apple iPhone 5s";
		dataBitSet[11] = 0b110000010000010001000001001000000000L;
		name[12] = "Apple iPhone 5c";
		dataBitSet[12] = 0b101000010000010001000001010000000000L;
		name[13] = "Apple iPhone 5";
		dataBitSet[13] = 0b110000010000010001000001100000000000L;
		name[14] = "LG G3";
		dataBitSet[14] = 0b100010000001000100101100000000000010L;
		name[15] = "LG G2";
		dataBitSet[15] = 0b100100000010001000101100000000000010L;
		name[16] = "LG Nexus 5";
		dataBitSet[16] = 0b101000000010001001001010000000010000L;
		name[17] = "LG Nexus 4";
		dataBitSet[17] = 0b101000000100001001001100000000100000L;
		name[18] = "Sony Experia Z1";
		dataBitSet[18] = 0b100000100010001000011010000000000010L;
		name[19] = "Sony Experia Z2";
		dataBitSet[19] = 0b100001000010000100011100000000000001L;
		name[20] = "Sony Experia Z3";
		dataBitSet[20] = 0b100010000010000100011010000000000001L;
		name[21] = "Sony Experia Z3 Compact";
		dataBitSet[21] = 0b101000001000001000011010000000001000L;
		// add spec
		spec[0] = "110g";
		spec[1] = "130g";
		spec[2] = "145g";
		spec[3] = "150g";
		spec[4] = "160g";
		spec[5] = "175g";
		spec[6] = "4.0";
		spec[7] = "4.5";
		spec[8] = "4.7";
		spec[9] = "5.1";
		spec[10] = "5.5";
		spec[11] = "5.7";
		spec[12] = "1G";
		spec[13] = "2G";
		spec[14] = "3G";
		spec[15] = "5Mp";
		spec[16] = "8Mp";
		spec[17] = "13Mp";
		spec[18] = "16Mp";
		spec[19] = "NFC";
		spec[20] = "a442";
		spec[21] = "a444";
		spec[22] = "i8";
		spec[23] = "1400";
		spec[24] = "1500";
		spec[25] = "1600";
		spec[26] = "1800";
		spec[27] = "1900";
		spec[28] = "2000";
		spec[29] = "2200";
		spec[30] = "2400";
		spec[31] = "2600";
		spec[32] = "2800";
		spec[33] = "3000";
		spec[34] = "3200";
		
	}
	
}
