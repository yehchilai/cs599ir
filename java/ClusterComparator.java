import java.util.Comparator;

/**
 *  compare two cluster by their score
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class ClusterComparator implements Comparator<Clustering> {
	/***********************
	 *  clustering score comparator 
	 * 
	 * @param	o1	a clustering score
	 * @param	o2	a clustering score
	 * @return	int	the result of comparator
	 * **********************/
	@Override
	public int compare(Clustering o1, Clustering o2) {
		// TODO Auto-generated method stub
		if(o1.clusterScore < o2.clusterScore)
			return 1;
		if(o1.clusterScore > o2.clusterScore)
			return -1;
		return 0;
	}
}
