import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.Reader;
import java.util.Arrays;
import java.util.Random;

public class BiClustering {
	public boolean[][] data = new boolean[22][35]; // data[phone][spec]
	public String[] phones_list = new String[22];
	public String[] specs_list = new String[35];
	public boolean[] test = new boolean[36];
	public int phone_size = 22;
	public int specs_size = 35;
	/*************
	 * Constructor
	 * **********/
	public BiClustering()
	{
		Scanner scanner = new Scanner( System.in );
		while(true)
		{
			System.out.println("Enter 1) full iteration   2) random[100,000]   3) phone only   4) specs only  5) back: ");
			System.out.printf("=> ");
			String input = scanner.next();
			if(Integer.parseInt(input) == 5)
				break;
			readData("test.txt");
			runAlgorithm(Integer.parseInt(input), true);
			System.out.println();
		}
	}
	public BiClustering(boolean[][] input, int a, int b)
	{
		phone_size = a;
		specs_size = b;
		data = new boolean[phone_size][specs_size];
		phones_list = new String[phone_size];
		specs_list = new String[specs_size];
		data = input.clone();
	}
	/*************
	 * Constructor
	 * **********/
	public BiClustering(boolean[][] input)
	{
		data = input;
	}
	
	public void runAlgorithm(int input , boolean print)
	{
		switch(input)
		{
			case 1: // full iteration through all phones & specs
				for(int i = 0; i < 1; i++)
				{
					for(int phone = 0; phone < phone_size; phone++) // sort phones by similarity
					{
						for(int spec = 0; spec < specs_size; spec++) // sort specifications by similarity
						{
							sortPhone(phone);
							sortSpecs(spec);
						}
					}
				}
				break;
			case 2: // randomly select 100,000 phone/specs
				Random rand = new Random();
				for(int x = 0; x < 100000; x++)
				{
					int i = rand.nextInt(phone_size); // randomly select a phone
					int j = rand.nextInt(specs_size); // randomly select a spec
					sortPhone(i); // sort phones by similarity to phone[i]
					sortSpecs(j); // sort specs by similarity to spec[j]
				}
				break;
			case 3: // cluster by phone similarity
				for(int phone = 0; phone < phone_size; phone++) // sort phones by similarity
				{
					sortPhone(phone);
				}
				break;
			case 4: // cluster by specs similarity
				for(int spec = 0; spec < specs_size; spec++) // sort specifications by similarity
				{
					sortSpecs(spec);
				}
				break;
		}
		
		/*
		for(int spec = 0; spec < specs_size; spec++) // sort specifications by similarity
		{
			sortSpecs(spec);
			for(int phone = 0; phone < phone_size; phone++) // sort phones by similarity
			{
				sortPhone(phone);
			}
		}*/
		
		/*for(int phone = 0; phone < phone_size; phone++) // sort phones by similarity
		{
			sortPhone(phone);
		}*/
		
		/*for(int spec = 0; spec < specs_size; spec++) // sort specifications by similarity
		{
			sortSpecs(spec);
		}*/
		
		if(print)
			printMatrix();
	}
	public void printMatrix()
	{
		//Scanner scanner = new Scanner( System.in );
		System.out.print("    ");
		for(int i = 0; i<specs_size; i++)
			System.out.printf("%3s", specs_list[i]);
		System.out.println();
		System.out.println();
		for(int phone = 0; phone < phone_size; phone++) // print matrix
		{
			System.out.printf("%2s", phones_list[phone]);
			System.out.print("  ");
			for(int spec = 0; spec < specs_size; spec++) 
			{
				if(data[phone][spec])
					System.out.printf("%3s", "1");
				else
					System.out.printf("%3s", " ");
			}
			System.out.println();
		}
		/*String input = scanner.next();
		if(Integer.parseInt(input) == 1)
			System.out.println("Next");*/
	}
	public void sortPhone(int phone) // sort rows base on similarity to phone
	{
		Integer[][] sim_scores = new Integer[phone_size][2];
		BitSet a = toBitSet(getRow(data, phone));
		for(int i = 0; i < phone_size; i++) // generate similarity score matrix
		{
			BitSet b = toBitSet(getRow(data, i));
			sim_scores[i][0] = i; // row_index
			sim_scores[i][1] = similarity(a, b); // similarity score
		}
		Arrays.sort(sim_scores, new ArrayComparator(1, false));
	
		int highest = sim_scores[1][0];
		boolean [][] temp = new boolean[data.length][];
		String[] phones_list_temp = new String[phone_size];
		int i = 0;
		//System.out.println("    phone = "+phone+"   highest = "+highest);
		if(phone != highest)
		{
			for(int j = 0; j  < sim_scores.length; j++)
			{
				if(i == phone)
				{
					i++;
					temp[j] = data[i].clone();
					phones_list_temp[j] = phones_list[i];
					//System.out.println("j = "+j+"   i = "+i);
					if(i == highest)
					{
						i++;
						j++;
						temp[j] = data[phone].clone();
						phones_list_temp[j] = phones_list[phone];
						//System.out.println("j = "+j+"   i = "+phone);
					}
					else
						i++;
				}
				else if(i == highest)
				{
					//System.out.println("beep");
					temp[j] = data[i].clone();
					phones_list_temp[j] = phones_list[i];
					//System.out.println("j = "+j+"   i = "+i);
					j++;
					i++;
					temp[j] = data[phone].clone();
					phones_list_temp[j] = phones_list[phone];
					//System.out.println("j = "+j+"   i = "+phone);
				}
				else
				{
					temp[j] = data[i].clone();
					phones_list_temp[j] = phones_list[i];
					//System.out.println("j = "+j+"   i = "+i);
					i++;
				}
			}
			data = temp.clone();
			phones_list = phones_list_temp.clone();
		}
	}
	
	// sort column
	public void sortSpecs(int spec) // sort rows base on similarity to phone
	{
		Integer[][] sim_scores = new Integer[specs_size][2];
		
		BitSet a = toBitSet(getCol(data, spec));
		for(int i = 0; i < specs_size; i++) // generate similarity score matrix
		{
			BitSet b = toBitSet(getCol(data, i));
			sim_scores[i][0] = i; // row_index
			sim_scores[i][1] = similarity(a, b); // similarity score
		}
		Arrays.sort(sim_scores, new ArrayComparator(1, false));
		
		boolean [][] temp = new boolean[phone_size][specs_size];
		String[] specs_temp = new String[specs_size];
		int i = 0;
		int highest = sim_scores[1][0];
		if(spec != highest)
		{
			for(int j = 0; j < sim_scores.length; j++)
			{
					//temp[phone][j] = data[phone][sim_scores[j][0]];
					if(i == spec)
					{
						i++;
						specs_temp[j] = specs_list[i];
						for(int phone = 0; phone < phone_size; phone++)
							temp[phone][j] = data[phone][i];
						//System.out.println("j = "+j+"   i = "+i);
						if(i == highest)
						{
							i++;
							j++;
							specs_temp[j] = specs_list[spec];
							for(int phone = 0; phone < phone_size; phone++)
								temp[phone][j] = data[phone][spec];
							//System.out.println("j = "+j+"   i = "+phone);
						}
						else
							i++;
					}
					else if(i == highest)
					{
						//System.out.println("beep");
						specs_temp[j] = specs_list[i];
						for(int phone = 0; phone < phone_size; phone++)
							temp[phone][j] = data[phone][i];
						//System.out.println("j = "+j+"   i = "+i);
						j++;
						i++;
						specs_temp[j] = specs_list[spec];
						for(int phone = 0; phone < phone_size; phone++)
							temp[phone][j] = data[phone][spec];
						//System.out.println("j = "+j+"   i = "+phone);
					}
					else
					{
						specs_temp[j] = specs_list[i];
						for(int phone = 0; phone < phone_size; phone++)
							temp[phone][j] = data[phone][i];
						//System.out.println("j = "+j+"   i = "+i);
						i++;
					}
			}
			data = temp.clone();
			specs_list = specs_temp.clone();
		}
	}
	
	
	public void readData(String fileName)
	{
	    File file = new File(fileName);

	    try {
	        Scanner sc = new Scanner(file);
	        
			for(int phone = 0; phone < phone_size; phone++) // sort phones by similarity
			{
				String temp = sc.next();
				for(int spec = 0; spec < specs_size; spec++) // sort specifications by similarity
				{
					if(temp.charAt(spec) == '0')
						data[phone][spec] = false;
					else if(temp.charAt(spec) == '1')
						data[phone][spec] = true;
				}
			}
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }
	    for(int i=0;i<phone_size;i++)
	    {
	    	phones_list[i] = Integer.toString(i);
	    }
	    for(int i=0;i<specs_size;i++)
	    {
	    	specs_list[i] = Integer.toString(i);
	    }
	 }
		
	public BitSet toBitSet(boolean[] input)
	{
		//BitSet result = new BitSet(input.length);
		BitSet result = new BitSet(input.length);
		for(int i = 0; i < input.length; i++)
		{
			result.set(i, input[i]);
		}
		
		/*System.out.println(result);
		for(int j = 0; j <  input.length; j++)
		{
			if(result.get(j))
				System.out.print("1");
			else
				System.out.print("0");
		}
		System.out.println();*/
		
		return result;
	}

	
	public boolean[] getCol(boolean[][] data, int col)
	{
		boolean[] result = new boolean[phone_size];
		for(int row = 0; row < result.length; row++)
		{
		    result[row] = data[row][col];
		}
		return result;
	}
	public boolean[] getRow(boolean[][] data, int row)
	{
		return data[row];
	}
	
	public int similarity(BitSet a, BitSet b)
	{
		int result = 0;
		
		BitSet bits = (BitSet) a.clone();
		bits.and(b);
		for(int i = bits.length() ; i >= 0 ; i--)
		{
			if(bits.get(i))
			{
				result++;
			}
		}
		
		return result;
	}
}
