import java.util.Scanner;

/**
 * 	Main function.
 *  Provide simple command to select different functionalities of our system.
 *  Select 1 to show a bi-clustering result.
 *  Select 2 to shoe the genetic algorithm process.
 *  Select 3 to exit.
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* manual input cluster score */
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner( System.in );
		while(true)
		{
			System.out.println("Enter 1) bi-clistering 2) bi-clustering with GA 3) exit :");
			System.out.printf("=> ");
			String input = scanner.next();
			if(GeneticAlgorithm.isInteger(input))
			{
				if(Integer.parseInt(input) == 1)
				{
					BiClustering b = new BiClustering();

				}else if(Integer.parseInt(input) == 2)
				{
					int p = 0;
					int g = 0;
					while(true)
					{
						System.out.println("Enter the number of population :");
						System.out.printf("=> ");
						String inputGA = scanner.next();
						if(GeneticAlgorithm.isInteger(inputGA))
						{
							p = Integer.parseInt(inputGA);
							break;
						}
					}
					
					while(true)
					{
						System.out.println("Enter the number of generation :");
						System.out.printf("=> ");
						String inputGA = scanner.next();
						if(GeneticAlgorithm.isInteger(inputGA))
						{
							g = Integer.parseInt(inputGA);
							break;
						}
						
					}
					GeneticAlgorithm ga = new GeneticAlgorithm(p);
					ga.runAlgorithm(g);
				}else if(Integer.parseInt(input) == 3)
				{
					break;
				}else
				{
					System.out.println("Wrong command! Please try again.");
				}
			}else
			{
				System.out.println("Wrong command! Please try again.");
			}
		}
		
	}
	
}
