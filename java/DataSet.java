import java.util.Arrays;
import java.util.BitSet;

/**
 *  The basic data structure for manipulating phone's specification
 */

/**
 * @author Yeh-chi Lai
 *
 */

public class DataSet {
	
	/* phone name */
	public String id;
	/* phone features */
	public BitSet data;
	
	/*************
	 * Constructor
	 * @param	str		id
	 * **********/
	public DataSet(String str)
	{
		this.id = str;
		this.data = new BitSet(36);
		
	}
	
	/*************
	 * Constructor
	 * @param	str		id
	 * @param	data	BitSet
	 * **********/
	public DataSet(String str, long data)
	{
		this.id = str;
		this.data = convert(data);
	}
	
	/*************
	 * Constructor
	 * @param	str		id
	 * @param	data	BitSet
	 * **********/
	public DataSet(String str, BitSet data)
	{
		this.id = str;
		this.data = data;
	}
	
	/*************
	 * convert long to BitSet
	 * @param	data		long 
	 * @return	BitSet		the BitSet converted from long
	 * **********/
	private BitSet convert(long data)
	{
		BitSet tmp = new BitSet(36);
		int index = 0;
	    while (data != 0L) 
	    {
		    if (data % 2L != 0) 
		    {
		      tmp.set(index);
		    }
		    ++index;
		    data = data >>> 1;
	    }
		return tmp;
	}
	
	/*************
	 * override equals
	 * @param	obj			compare object
	 * @return	boolean		if they are equal and have the same hash code, return true
	 * **********/
	@Override
    public boolean equals(Object obj) 
	{
        return obj instanceof DataSet && obj.hashCode() == hashCode();
    }
	
	/*************
	 * override hashcode
	 * @return	int			hash code
	 * **********/
	@Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { id});
    }
}
