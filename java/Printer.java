import java.util.BitSet;
import java.util.List;

/**
 * 	Printer utilities.
 *  provide debug print and result print
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class Printer {

	public Printer()
	{
		
	}
	
	/*************
	 * print IDs in the list data set (from the bitset length to zero)
	 * @param	p	a list data set which should be printed
	 * **********/
	public static void printIDs(List<DataSet> p)
	{
		System.out.println("Data size : " + p.get(0).data.length());
		System.out.println("P size : " + p.size());
		for(int i = 0 ; i < p.size() ; i++)
		{
			System.out.printf("%30s", p.get(i).id);
			printData(p.get(i).data);
			System.out.println();
		}
	}
	
	/*************
	 * print ID only in the list data set (from the bitset length to zero)
	 * @param	p	a list data set which should be printed
	 * **********/
	public static void printID(List<DataSet> p)
	{
		for(int i = 0 ; i < p.size() ; i++)
		{
			System.out.printf("%7s", p.get(i).id);
		}
		System.out.println();
	}
	
	/*************
	 * print IDs in the list data set (from zero to bitset length)
	 * @param	p	a list data set which should be printed
	 * **********/
	public void printIDsFromZero(List<DataSet> p)
	{
		System.out.println("Data size : " + p.get(0).data.length());
		System.out.println("P size : " + p.size());
		for(int i = 0 ; i < p.size() ; i++)
		{
			System.out.printf("%30s", p.get(i).id);
			printDataFromZero(p.get(i).data);
			System.out.println();
		}
	}
	
	/*************
	 * print BitSet, the specific data in the list data set 
	 * (from the bitset length to zero)
	 * @param		data	a BitSet which need to be printed
	 * **********/
	public static void printData(BitSet data)
	{
		for(int i = data.length() - 2 ; i >= 0 ; i--)
		{
			if(data.get(i))
			{
				System.out.printf("%7s", "1");
			}else
			{
				System.out.printf("%7s", "0");
			}
			
		}
	}
	
	/*************
	 * print BitSet, the specific data in the list data set
	 * (from zero to bitset length)
	 * @param		data	a BitSet which need to be printed
	 * **********/
	public static void printDataFromZero(BitSet data)
	{
		for(int i = 0 ; i < data.length() - 1 ; i++)
		{
			if(data.get(i))
			{
				System.out.printf("%7s", "1");
			}else
			{
				System.out.printf("%7s", " ");
			}
			
		}
	}
	
	/*************
	 * print bi-Clustering result
	 * @param	phoneName	a list of phone names
	 * @param	data		a list data set which has completed bi-clustering process
	 * **********/
	public static void printResult(List<String> phoneName, List<DataSet> data)
	{
		System.out.printf("%30s", "");
		
		for(int i = 0 ; i < data.size() ; i++)
		{
			System.out.printf("%7s", data.get(i).id);
		}
		
		System.out.println();
		for(int i = 0 ; i < phoneName.size() ; i++)
		{
			System.out.printf("%30s", phoneName.get(i));
			for(int j = 0; j < data.size() ; j++)
			{
				if(data.get(j).data.get(i) == true)
				{
					System.out.printf("%7s", "1");
				}else
				{
					System.out.printf("%7s", " ");
				}
				
			}
			System.out.println();
		}
		
		printResultScaled(phoneName, data);
	}
	
	/*************
	 * print bi-Clustering result
	 * @param	phoneName	a list of phone names
	 * @param	data		a list data set which has completed bi-clustering process
	 * **********/
	public static void printResultScaled(List<String> phoneName, List<DataSet> data)
	{
		System.out.println("=============  Scaled  ========================");
		for(int i = 0 ; i < phoneName.size() ; i++)
		{
			for(int j = 0; j < data.size() ; j++)
			{
				if(data.get(j).data.get(i) == true)
				{
					System.out.printf("%3s", "*");
				}else
				{
					System.out.printf("%3s", " ");
				}
				
			}
			System.out.println();
		}
	}
	
	/*************
	 * print original data list
	 * @param	specs		a list of phone specification
	 * @param	data		a original list data set
	 * **********/
	public static void printOriginalData(String[] specs, List<DataSet> data)
	{
		System.out.printf("%30s", "");
		
		for(int i = 0 ; i < specs.length ; i++)
		{
			System.out.printf("%7s", specs[specs.length -1 - i]);
		}
	
		System.out.println();
		for(int j = 0 ; j < data.size() ; j++)
		{
			System.out.printf("%30s", data.get(j).id);
			printDataFromZero(data.get(j).data);
			System.out.println();
		}
		
		//printOriginalDataScaled(specs, data);
	}
	
	public static void printOriginalDataScaled(String[] specs, List<DataSet> data)
	{
		System.out.println("================== Scaled =============");
		for(int j = 0 ; j < data.size() ; j++)
		{
			printDataFromZeroScaled(data.get(j).data);
			System.out.println();
		}
	}
	
	/*************
	 * print BitSet, the specific data in the list data set
	 * (from zero to bitset length)
	 * @param		data	a BitSet which need to be printed
	 * **********/
	public static void printDataFromZeroScaled(BitSet data)
	{
		for(int i = 0 ; i < data.length() - 1 ; i++)
		{
			if(data.get(i))
			{
				System.out.printf("%3s", "*");
			}else
			{
				System.out.printf("%3s", " ");
			}
			
		}
	}
	
	
	/*************
	 * print random specification
	 * @param	specs		a list of phone specification
	 * **********/
	public void printRandomSpec(String[] specs)
	{
		for(int i = 0 ; i < specs.length ; i++)
		{
			System.out.printf("%7s", specs[i]);
		}
	}
	
	/*************
	 * print random specification
	 * @param	specs		a list of phone specification
	 * **********/
	public void printRandomSpec(List<String> specs)
	{
		String[] output = new String[specs.size()];
		specs.toArray(output);
		for(int i = 0 ; i < output.length ; i++)
		{
			System.out.printf("%7s", output[i]);
		}
	}
}
