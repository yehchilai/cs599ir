import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/**
 *  Genetic algorithm functions.
 *  Provide basic genetic algorithm operations such as
 *  population, selection, and crossover
 */

/**
 * @author Yeh-chi Lai
 *
 */
public class GeneticAlgorithm {
	
	/* population */
	private int population;
	
	/******************************************
	 * Constructor
	 * @param	population	the total population(clustering) 
	 * 						that we will put in the genetic algorithm
	 * ****************************************/
	public GeneticAlgorithm(int population)
	{
		this.population = population;
	}
	
	/******************************************
	 * Run genetic algorithm
	 * @param	generation		the total generations we will run
	 * ****************************************/
	public void runAlgorithm(int generation)
	{
		/* cluster generator */
		InputGenerator newInput = new InputGenerator();
		/* manual input cluster score */
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner( System.in );
		
		/* selection number*/
		int selectionNumber = (int)(this.population*0.2f); // only select top 20% 
		if(selectionNumber%2 == 1)
		{
			selectionNumber++;
		}
		if(selectionNumber <= 2 && population >=2)
			selectionNumber = 2;
		
		/* queens of population */
		PriorityQueue<Clustering> clusters = new PriorityQueue<Clustering>(11, new ClusterComparator());	
		for(int i = 0 ; i < population ; i++)
		{
			//Clustering newClusterResult = newInput.generateCluster(35);
			Clustering newClusterResult = newInput.generateBiCluster(35, 1);
			/* evaluation function (manual input)*/
			while(true)
			{
				Printer.printResult(newClusterResult.resultNames, newClusterResult.resultBiCluster);
				//Printer.printOriginalData(newClusterResult.spec, newClusterResult.myData);
				System.out.println("Please give this cluster a score : ");
				System.out.printf("=> ");
				String inputScore = scanner.next();
				if(isInteger(inputScore))
				{
					newClusterResult.clusterScore = Integer.parseInt(inputScore);
					clusters.add(newClusterResult);
					break;
				}
			}
			
		}
		/* run genetic algorithm operation and check if it has optimal solution */
		int g = generation;
		while(g > 0)
		{
			System.out.println("===== Generation "+ (generation - g + 1) + " =====");
			clusters = selection(selectionNumber, clusters);
			g--;
		}
		
		/* show the best cluster */
		System.out.println("Best Cluster Feature Result is this : ");
		Clustering result = clusters.poll();
		System.out.println("The best Cluster Score = " + result.clusterScore);
		Printer.printResult(result.resultNames, result.resultBiCluster);
	}
	
	/******************************************
	 * Selection 
	 * @param	selectionNumber				the number of chromosome which is pull from population
	 * @param	clusters					the population
	 * @return	PriorityQueue<Clustering>	the next generation population
	 * ****************************************/
	public PriorityQueue<Clustering> selection(int selectionNumber, PriorityQueue<Clustering> clusters)
	{
		PriorityQueue<Clustering> result = new PriorityQueue<Clustering>(11, new ClusterComparator());
		PriorityQueue<Clustering> resultTmp = new PriorityQueue<Clustering>(11, new ClusterComparator());
		Clustering[] clustersTmp = clusters.toArray(new Clustering[0]);
		
		/* manual input cluster score */
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner( System.in );
		
		/* select top chromosome to crossover */
		for(int i = 0 ; i < selectionNumber ; i++ )
		{
			Clustering[] tmp = crossover(clustersTmp[i], clustersTmp[i + 1]);
			
			/* original generation */
			resultTmp.add(clustersTmp[i]);
			resultTmp.add(clustersTmp[i + 1]);
			/* next generation */
			//evaluation function (manual input). Evaluate the first child
			//tmp[0].runAlgorithm(tmp[0].getData(), true);
			tmp[0] = InputGenerator.convertClusterByBiCluster(tmp[0], 1, tmp[0].spec, clustersTmp[i].resultNames.toArray(new String[clustersTmp[i].resultNames.size()]));
			Printer.printResult(tmp[0].resultNames, tmp[0].resultBiCluster);
			///////////////////////
			
			///////////////////////
			while(true)
			{
				System.out.println("Please give this cluster a score : ");
				System.out.printf("=> ");
				String inputScore = scanner.next();
				if(isInteger(inputScore))
				{
					tmp[0].clusterScore = Integer.parseInt(inputScore);
					resultTmp.add(tmp[0]);
					break;
				}
				
			}
			// evaluation function (manual input). Evaluate the Second child
			tmp[1] = InputGenerator.convertClusterByBiCluster(tmp[1], 1, tmp[1].spec, clustersTmp[i].resultNames.toArray(new String[clustersTmp[i].resultNames.size()]));
			Printer.printResult(tmp[1].resultNames, tmp[1].resultBiCluster);
			while(true)
			{
				//tmp[1].runAlgorithm(tmp[1].getData(), true);
				System.out.println("Please give this cluster a score : ");
				System.out.printf("=> ");
				String inputScore = scanner.next();
				if(isInteger(inputScore))
				{
					tmp[1].clusterScore = Integer.parseInt(inputScore);
					resultTmp.add(tmp[1]);
					break;
				}
			}

			i++;
		}
		
		/* add the rest of chromosome */
		for(int i = selectionNumber ; i < clustersTmp.length ; i++)
		{
			resultTmp.add(clustersTmp[i]);
		}
		
		/* only return the same number of population */
		Clustering[] clustersResultTmp = resultTmp.toArray(new Clustering[0]);
		for(int i = 0 ; i < population ; i++)
		{
			result.add(clustersResultTmp[i]);
		}
		
		return result;
	}
	
	/******************************************
	 * Crossover 
	 * @param	a				a chromosome which contains a set of specification
	 * @param	b				a chromosome which contains a set of specification
	 * @return	Clustering		a new chromosome which contains a new set of specification
	 * ****************************************/
	public Clustering[] crossover(Clustering a, Clustering b)
	{
		Clustering[] result = new Clustering[2];	
		/* set two new cluster for crossover */
		List<String> phoneNamesA = a.resultNames;
		List<DataSet> clusterA = a.resultBiCluster;
		String[] specA = setSpec(clusterA);
		
		List<String> phoneNamesB = b.resultNames;
		List<DataSet> clusterB = b.resultBiCluster;
		String[] specB = setSpec(clusterB);
		
		Clustering newClusterATmp = convertToHorizontal(phoneNamesA, clusterA, specA);
		Clustering newClusterBTmp = convertToHorizontal(phoneNamesB, clusterB, specB);
		/* sort phone names */
		Collections.sort(newClusterATmp.myData, new DataSetComparator());
		Collections.sort(newClusterBTmp.myData, new DataSetComparator());
	
		/* crossover by random cut point */
		Random random = new Random();
		int cutPointA = random.nextInt(clusterA.size());
		int cutPointB = random.nextInt(clusterB.size());
		
		result[0] = merge(0, cutPointA, newClusterATmp, 0, cutPointB, newClusterBTmp);
		result[1] = merge(cutPointA, clusterA.size(), newClusterATmp, cutPointB, clusterB.size(), newClusterBTmp);

		return result;
	}
	
	/******************************************
	 * set specification by a list of data 
	 * @param	data			a list of data contain specification
	 * @return	String[]		a specification string array
	 * ****************************************/
	public static String[] setSpec(List<DataSet> data)
	{
		String[] newSet = new String[data.size()];
		for(int i = 0; i < newSet.length ; i++)
		{
			newSet[i] = data.get(i).id;
		}
		
		return newSet;
	}
	
	/******************************************
	 * convert the list of data set to horizontal BitSet with phone name id
	 * @param	phoneNames			a list of phone name
	 * @param	cluster				a list of data set
	 * @param	spec				a specification array
	 * @return	Clustering			a new cluster which contain a data set with phone name id
	 * ****************************************/
	public static Clustering convertToHorizontal(List<String> phoneNames, List<DataSet> cluster, String[] spec)
	{
		Clustering newClusterTmp = new Clustering(spec.length, spec);

		for(int i = 0 ; i < phoneNames.size() ; i++)
		{
			BitSet tmpA = new BitSet();
			for(int j = 0 ; j < cluster.size() ; j++)
			{
				tmpA.set(j, cluster.get(cluster.size() - 1 - j).data.get(i));
			}
			tmpA.set(cluster.size(), true);
			newClusterTmp.addBitSet(phoneNames.get(i), InputGenerator.convertToLong(tmpA));
		}
		
		return newClusterTmp;
	}
	
	/******************************************
	 * convert the list of data set to vertical BitSet with Specification id
	 * @param	tmpResult			a list of data set which contains phone name as id
	 * @param	spec				a specification array
	 * @return	List<DataSet>		a new data set which contain specification as id
	 * ****************************************/
	public ArrayList<DataSet> convertToVertical(List<DataSet> tmpResult, String[] spec)
	{
		ArrayList<DataSet> tmp = new ArrayList<DataSet>();
		for(int i = 0; i < tmpResult.get(0).data.length() - 1 ; i++)
		{
			BitSet tmpBitSet = new BitSet();
			for(int j = 0; j < 22 ; j++)
			{
				tmpBitSet.set(j, tmpResult.get(j).data.get(i));
				
			}
			tmpBitSet.set(22, true);
			tmp.add(new DataSet(spec[tmpResult.get(0).data.length() - 2 - i], tmpBitSet));
		}
		
		return tmp;
	}
	
	/******************************************
	 * get a phone name list by a list of data set
	 * @param	tmpResult			a list of data
	 * @return	List<String>		a list of phone name
	 * ****************************************/
	public List<String> phoneNameList(List<DataSet> tmpResult)
	{
		List<String> tmp = new ArrayList<String>();
		for(int i = 0 ; i < tmpResult.size(); i++)
		{
			tmp.add(tmpResult.get(i).id);
		}
		
		return tmp;
	}
	
	/******************************************
	 * get a phone name list by a list of data set
	 * @param	tmpResult			a list of data
	 * @return	List<String>		a list of phone name
	 * ****************************************/
	public Clustering merge(int startA, int endA, Clustering newClusterATmp, int startB, int endB, Clustering newClusterBTmp)
	{
		ArrayList<DataSet> aList = convertToVertical(newClusterATmp.myData, newClusterATmp.spec);
		ArrayList<DataSet> bList = convertToVertical(newClusterBTmp.myData, newClusterBTmp.spec);
		
		List<DataSet> aListTmp = aList.subList(startA, endA);
		List<DataSet> bListTmp = bList.subList(startB, endB);
		
		Set<DataSet> aSet = new HashSet<DataSet>(aListTmp);
		Set<DataSet> bSet = new HashSet<DataSet>(bListTmp);
		
		/* print subsets */
//		System.out.println("#################### Part of the spec set######################");
//		Printer.printID(new ArrayList<DataSet>(aSet));
//		System.out.println("#################### Part of the spec set######################");
//		Printer.printID(new ArrayList<DataSet>(bSet));
		
		aSet.addAll(bSet);
		ArrayList<DataSet> resultTmp = new ArrayList<DataSet>(aSet);
		
		return convertToHorizontal(phoneNameList(newClusterATmp.myData), resultTmp, setSpec(resultTmp));
	}
	
	/******************************************
	 * check a string is a integer
	 * @param	s			a string
	 * @return	boolean		if it is a integer, return ture
	 * ****************************************/
	public static boolean isInteger(String s) 
	{
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) 
	    { 
	        return false; 
	    }
	    return true;
	}
}
